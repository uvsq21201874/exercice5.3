
public abstract class File {
	protected String nom;
	protected String path;
	protected int taille;
	protected boolean estfichier;
	protected boolean estrepertoire;
	
	
	abstract int mataille();
	
	protected String getpath(){
		return path;
	}
	
	public File creationFile(String nom,boolean fichier, boolean repertoire){
		if(fichier){
			return new Fichier(nom,this.path);
		}else{
			if(repertoire){
				return new Repertoire(nom,this.path);
			}
		}
		return null;
	}

	
}
