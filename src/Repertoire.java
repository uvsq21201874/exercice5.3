import java.util.ArrayList;

public class Repertoire extends File{
	private boolean racine;
	ArrayList<File> f = new ArrayList<File>();
	
	public Repertoire(){//création du répertoire racine
		this.nom="racine";
		this.estfichier=false;
		this.estrepertoire=true;
		this.taille=0;
		this.racine=true;
		this.path="/";
	}
	
	public Repertoire(String nom,String path){
		this.nom=nom;
		this.estfichier=false;
		this.estrepertoire=true;
		this.taille=this.mataille();
		this.racine=false;
		this.path=path;
	}
	
	
	
	
	public void ajouterFile(File f){
		this.f.add(f);
	}
	
	int mataille(){
		int res=0;
		for(File t : f){
			 res+=t.mataille();
		}
		return res;
	}
}
